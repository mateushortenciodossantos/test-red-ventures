const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractSass = new ExtractTextPlugin('./dist/main.css');

module.exports = {
  entry: ['./src/index.js', './scss/main.scss'],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader",
      //presets: [
        //[ "es2015", { "modules": false } ]
      //]
    },
    {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({fallback: "style-loader", use: [
        {loader: "css-loader", options: { minimize: true}},
        {loader: "sass-loader"}
      ]}),
      include: path.join(__dirname, "scss")
    },
    {
      test: /\.css$/,
      loader: ["style-loader", "css-loader"]
    },
      {
        test: /\.(png|jpg|svg)$/,
        include: path.join(__dirname, 'images'),
        loader: 'url-loader'
     }]
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'dist')
  },
  plugins: [
    new ExtractTextPlugin('main.css')
  ]
};
