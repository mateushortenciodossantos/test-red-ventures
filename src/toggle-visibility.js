const makeVisible = (element, transitionSeconds) => {
  if(element.classList.contains("-hidden")) {
    element.style.opacity = 0;
    element.classList.remove("-hidden");

    const timeToFade = setTimeout(() => {
      element.style.opacity = 1;
      clearTimeout(timeToFade);
    }, transitionSeconds);
  }
  else {
    element.style.opacity = 0;

    const timeToFade = setTimeout(() => {
      element.classList.add("-hidden")
      element.style.opacity = 1;
      clearTimeout(timeToFade);
    }, transitionSeconds);
  }
}

export default makeVisible;
