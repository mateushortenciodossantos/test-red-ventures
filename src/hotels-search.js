import $ from "jquery";
import toggleVisibility from "./toggle-visibility";
import { starsAmount, priceHistory, hotelCardTemplate, bindEvents} from "./card-events";

var searchState = { hotels: [], filteredHotels: [] };
const timeoutTransitionTime = 250;
var mainContent = $("#main-content");
var container = $(".hotel-cards-container");

const returnHotels = () => {
  $.ajax({
    url: "http://www.raphaelfabeni.com.br/rv/hotels.json",
    success: (res) => {
      searchState.hotels = res.hotels;
      searchState.filteredHotels = res.hotels;
      updateUI();
      bindEvents();
      toggleVisibility(mainContent[0], timeoutTransitionTime);
      $("html, body").animate({ scrollTop: mainContent.offset().top }, 1000);
    }
  });
}

const toCurrencyString = (price) => {
  return price.toFixed(2).replace(".", ",");
}

const updateUI = () => {
  const hotels = searchState.filteredHotels;
  if(!hotels.length) {
      container.html('<h2 class="no-result-message">No results found :(</h2>');
      showCardContainer();
      return;
  }

  const hotelsHtml = hotels.map((hotel) => {
    var rate = "";
    var priceHistoryValues = "";
    var priceHistoryLabels = "";
    for(var i = 0; i < hotel.rate; i++) {
        rate += starsAmount;
    }

    const higherPrice = Math.max.apply(Math, hotel.price_history.map((price) => price.value))
    for(var i = 0; i < hotel.price_history.length; i++) {
        const percentageValue = hotel.price_history[i].value*100/higherPrice;
        priceHistoryLabels += "<li>" + hotel.price_history[i].month + "</li>";
        priceHistoryValues += priceHistory.replace("{price}", hotel.price_history[i].value)
                                          .replace("{percentage}", percentageValue);
    }

    //TODO: make array with key instead of procedural replacement
    const hotelHtml = hotelCardTemplate.replace("{image}", hotel.image)
                                       .replace("{nameAlt}", hotel.name)
                                       .replace("{name}", hotel.name)
                                       .replace("{starsAmount}", rate)
                                       .replace("{description}", hotel.description)
                                       .replace("{price}", toCurrencyString(hotel.price))
                                       .replace("{priceHistoryValues}", priceHistoryValues)
                                       .replace("{priceHistoryLabels}", priceHistoryLabels);
    return hotelHtml;
  });

  showCardContainer();
  container.html(hotelsHtml.join(""));
}

const showCardContainer = () => {
  if(container.hasClass("-hidden"))
    toggleVisibility(container[0], timeoutTransitionTime);
}

//{ minPrice: 0, maxPrice: 650, starsAmount: 0 }
const filterHotels = (filter) => {
  searchState.filteredHotels = searchState.hotels.filter((hotel) => {
    if(hotel.price >= filter.minPrice && hotel.price <= filter.maxPrice && hotel.rate == filter.starsAmount)
      return hotel;
  });
  toggleCardContainerVisibility();
  let timeoutId = setTimeout(() => {
    updateUI();
    clearTimeout(timeoutId);
  }, timeoutTransitionTime);
}

const toggleCardContainerVisibility = () => {
  let container = $(".hotel-cards-container");
  toggleVisibility(container[0], timeoutTransitionTime);
}

module.exports = { returnHotels, filterHotels };
