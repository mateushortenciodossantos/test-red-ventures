const toCalendarString = (date) => {
  var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  var monthName = monthNames[date.getMonth()];
  var day = date.getDate();
  var year = date.getFullYear();

  return monthName + " " + day + ", " + year;
}

export default toCalendarString;
