import $ from 'jquery';
import toggleVisibility from './toggle-visibility';

const starsAmount = '<div class="icon -star -filled -smaller"></div>'
const priceHistory = '<li><div style="height:{percentage}%"><div class="tooltip">${price}</div></div></li>';
const hotelCardTemplate = `<div class="card">
  <img class="image" src="{image}" alt="{nameAlt}">
  <div class="description content">
    <div class="stars-container">
      {starsAmount}
    </div>
    <h2 class="text-orange">{name}</h2>
    <p class="text-gray">{description}</p>
    <div class="hotel-information">
      <button type="button" class="btn -orange" name="button">Book now</button>
      <button type="button" class="btn -green show-price-history" name="button">Price history</button>
    </div>
  </div>
  <div class="price -text-right">
    <p class="text-gray">Total</p>`
    + '<p class="text-green">${price}</p>' +
  `</div>
  <div class="price-history -hidden content -larger">
    <h2 class="text-orange">PRICE HISTORY FOR 2017</h2>
    <div class="graph">
      <ul class="values-container">
        {priceHistoryValues}
      </ul>
      <ul class="labels-container">
        {priceHistoryLabels}
      </ul>
    </div>
    <div class="back-button">
      <div class="icon -pointer-left">
      </div>
      <p class="text">Back to description</p>
    </div>
  </div>
</div>`;

const bindEvents = () => {
  const transitionTime = 250;
  $(document).on("click", ".back-button", (e) => {
    const card = $(e.currentTarget).closest(".card")
    toggleVisibility(card.find(".price-history")[0], transitionTime);
    const timeout = setTimeout(function () {
      card.find(".price").removeClass("-hidden");
      card.find(".description").removeClass("-hidden");
      clearTimeout(timeout);
    }, transitionTime);
  });

  $(document).on("click", ".show-price-history", (e) => {
    const card = $(e.currentTarget).closest(".card")
    toggleVisibility(card.find(".price")[0], transitionTime);
    toggleVisibility(card.find(".description")[0], transitionTime);
    const timeout = setTimeout(function () {
      card.find(".price-history").removeClass("-hidden");
      clearTimeout(timeout);
    }, transitionTime);
  });
}

module.exports = {starsAmount, priceHistory, hotelCardTemplate, bindEvents};
