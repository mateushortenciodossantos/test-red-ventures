import navbarScroll from './navbar-scroll';
import filterStore from './filter';
import datepicker from './date-picker';
import search from './hotels-search';
import cardEvents from './card-events';
