import $ from 'jquery';

window.onscroll = () => {
  if(window.scrollY > 0)
    $(".navbar-container").addClass("white");
  else
    $(".navbar-container").removeClass("white");
}

module.exports = "navbar-scroll";
