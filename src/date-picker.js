import $ from "jquery";
import flatpickr from 'flatpickr';
import 'flatpickr/dist/flatpickr.min.css';
import toCalendarString from "./to-calendar-date";
import { returnHotels } from "./hotels-search";

var datePickerStore = { checkIn: null, checkOut: null, showResults: false };

flatpickr("#datepicker-container", {
  inline: true,
  mode: "range",
  onChange: (selectedDates, dateStr, instance) => {
      var firstDate = selectedDates[0];
      datePickerStore.checkIn = firstDate;

      if(selectedDates.length > 1) {
        var lastDate = selectedDates[1];
        datePickerStore.checkOut = lastDate;
      }

    updateUI();
  }
});

$("#search-hotels").on("click", () => {
  const { checkIn, checkOut }  = datePickerStore;
  if(checkIn && checkOut) {
    datePickerStore.showResults = true;
    updateUI();
  }
});

const updateUI = () => {
  if(datePickerStore.checkIn) {
    var textCheckIn = toCalendarString(datePickerStore.checkIn);
    $("#checkin-date").text(textCheckIn);
  }

  if(datePickerStore.checkOut) {
    var textCheckOut = toCalendarString(datePickerStore.checkOut);
    $("#checkout-date").text(textCheckOut);
  }


  if(datePickerStore.showResults) {
    const { checkIn, checkOut }  = datePickerStore;

    const message = ["Best choices between {start} and {end}", "Best choices in {start}"]
    const newMessage = checkIn === checkOut
                    ? message[1].replace("{start}", toCalendarString(checkIn))
                    : message[0].replace("{start}", toCalendarString(checkIn)).replace("{end}", toCalendarString(checkOut));
    returnHotels();
    $("#main-content")[0].style.opacity = 1;
    $(".date-message").text(newMessage);
  }
}
