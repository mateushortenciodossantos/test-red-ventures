import $ from 'jquery';
import multipleInput from './multiple-range';
import { filterHotels } from './hotels-search';

multipleInput();
var debounceTimeout = undefined;
var filterStore = { minPrice: 0, maxPrice: 650, starsAmount: 0 };

$(document).on('input change', '.price-range.original', function() {
    var price = $(this).val().split(",")[0];
    filterStore.minPrice = price;
    updateMinPriceUI();
    debounce();
});

$(document).on('input change', '.price-range.ghost', function() {
    filterStore.maxPrice = $(this).val();
    updateMaxPriceUI();
    debounce();
});

$(".filter-container > .stars-container > .-star").on("click", (e) => {
  filterStore.starsAmount = e.currentTarget.dataset.amount;
  updateStarsUI();
  debounce();
});

const updateMinPriceUI = () => {
  $("#min-price").text("$" + filterStore.minPrice);
}

const updateMaxPriceUI = () => {
  $("#max-price").text("$" + filterStore.maxPrice);
}

const updateStarsUI = () => {
  var stars = $(".filter-container > .stars-container > .-star");
  stars.map((index, star) => {
    if(star.dataset.amount <= filterStore.starsAmount) {
      star.classList.add("-filled");
    }
    else {
      star.classList.remove("-filled");
    }
  })
}

const debounce = () => {
  clearTimeout(debounceTimeout)
  debounceTimeout = setTimeout(() => {
    filterHotels(filterStore);
  }, 1000);
}

updateMaxPriceUI();
updateMinPriceUI();
